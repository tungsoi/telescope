<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Bamboo\Admin\Controllers\Dashboard;
use Bamboo\Admin\Layout\Column;
use Bamboo\Admin\Layout\Content;
use Bamboo\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Dashboard')
            ->description('Description...')
            ->row(Dashboard::title())
            ->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::environment());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::extensions());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::dependencies());
                });
            });
    }
}
