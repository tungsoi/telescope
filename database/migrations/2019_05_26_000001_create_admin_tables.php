<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
        return config('admin.database.connection') ?? config('database.default');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable(config('admin.database.users_table')))
        {
            Schema::create(config('admin.database.users_table'), function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('email', 190)->unique();
                $table->string('password', 60);
                $table->string('name');
                $table->string('avatar')->nullable();
                $table->string('remember_token', 100)->nullable();
                $table->string('provider_id', 190)->nullable()->unique();
                $table->string('provider', 50)->nullable();
                $table->tinyInteger('is_social')->nullable();
                $table->date('created_date')->nullable();
                $table->text('token')->nullable();
                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.roles_table')))
        {
            Schema::create(config('admin.database.roles_table'), function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('name', 50)->nullable();
                $table->string('slug', 50)->nullable();
                $table->text('http_path')->nullable();
                $table->string('http_method')->nullable();
                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.permissions_table')))
        {
            Schema::create(config('admin.database.permissions_table'), function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('name', 50)->unique()->nullable();
                $table->string('slug', 50)->unique()->nullable();
                $table->text('http_path')->nullable();
                $table->string('http_method')->nullable();
                $table->integer('permission_id')->nullable();

                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.menu_table')))
        {
            Schema::create(config('admin.database.menu_table'), function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('parent_id')->default(0);
                $table->integer('order')->default(0);
                $table->string('title', 50);
                $table->integer('type_id');
                $table->tinyInteger('is_header')->nullable();
                $table->string('icon', 50)->nullable();
                $table->string('uri', 50)->nullable();
                $table->string('permission')->nullable();
                $table->tinyInteger('is_active')->nullable();

                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.role_users_table')))
        {
            Schema::create(config('admin.database.role_users_table'), function(Blueprint $table)
            {
                $table->unsignedInteger('role_id');
                $table->unsignedInteger('user_id');
                $table->index([
                    'role_id',
                    'user_id'
                ]);
                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.role_permissions_table')))
        {
            Schema::create(config('admin.database.role_permissions_table'), function(Blueprint $table)
            {
                $table->unsignedInteger('role_id')->nullable();
                $table->unsignedInteger('permission_id')->nullable();
                $table->index([
                    'role_id',
                    'permission_id'
                ]);
                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.user_permissions_table')))
        {
            Schema::create(config('admin.database.user_permissions_table'), function(Blueprint $table)
            {
                $table->unsignedInteger('user_id');
                $table->unsignedInteger('permission_id');
                $table->index([
                    'user_id',
                    'permission_id'
                ]);
                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.role_menu_table')))
        {
            Schema::create(config('admin.database.role_menu_table'), function(Blueprint $table)
            {
                $table->unsignedInteger('role_id');
                $table->unsignedInteger('menu_id');
                $table->index([
                    'role_id',
                    'menu_id'
                ]);
                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.operation_log_table')))
        {
            Schema::create(config('admin.database.operation_log_table'), function(Blueprint $table)
            {
                $table->increments('id');
                $table->unsignedInteger('user_id');
                $table->string('path');
                $table->string('method', 10);
                $table->string('ip');
                $table->text('input');
                $table->index('user_id');
                $table->timestamps();
            });
        }

        if(! Schema::hasTable(config('admin.database.translations_table', 'admin_translations')))
        {
            Schema::create(config('admin.database.translations_table', 'admin_translations'), function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('status')->default(0);
                $table->string('locale');
                $table->string('group');
                $table->text('key');
                $table->text('value')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('admin.database.users_table'));
        Schema::dropIfExists(config('admin.database.roles_table'));
        Schema::dropIfExists(config('admin.database.permissions_table'));
        Schema::dropIfExists(config('admin.database.menu_table'));
        Schema::dropIfExists(config('admin.database.user_permissions_table'));
        Schema::dropIfExists(config('admin.database.role_users_table'));
        Schema::dropIfExists(config('admin.database.role_permissions_table'));
        Schema::dropIfExists(config('admin.database.role_menu_table'));
        Schema::dropIfExists(config('admin.database.operation_log_table'));
        Schema::dropIfExists(config('admin.database.translations_table', 'admin_translations'));
    }
}
