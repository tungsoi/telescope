<?php

return [
    'header'                    =>  'Laravel-Admin',
    'name'                      => 'Telescope',
    'logo'                      => '<img width="120" src="'.admin_base_path('bamboo-admin/img/telescope.png').'">',
    'logo-mini'                 => '<b>TL</b>',
    'bootstrap'                 => app_path('Admin/bootstrap.php'),
    'route'                     => [
        'prefix'     => env('ADMIN_ROUTE_PREFIX', 'admin'),
        'namespace'  => 'App\\Admin\\Controllers',
        'middleware' => [
            'web',
            'auth'
        ],
    ],
    'directory'                 => app_path('Admin'),
    'title'                     => 'Telescope',
    'https'                     => env('ADMIN_HTTPS', false),
    'auth'                      => [
        'controller'  => Bamboo\Admin\Controllers\AuthController::class,
        'guards'      => [
            'auth' => [
                'driver'   => 'session',
                'provider' => 'auth',
            ],
        ],
        'providers'   => [
            'auth' => [
                'driver' => 'eloquent',
                'model'  => Bamboo\Admin\Auth\Database\Administrator::class,
            ],
        ],
        // Add "remember me" to login form
        'remember'    => true,
        // Redirect to the specified URI when user is not authorized.
        'redirect_to' => 'auth/login',
        // The URIs that should be excluded from authorization.
        'excepts'     => [
            'auth/login',
            'auth/logout',
            'auth/login/bamboo',
            'auth/login/bamboo/callback',
            'auth/login/bamboo/logout',
            'auth/login/azure',
            'auth/login/azure/callback',
            'auth/login/azure/logout',
            'oauth/*',
        ],
    ],
    'form-style'                => 'xs',//lg,sm,xs
    'login'                     => [
        'email'  => env('AUTH_LOGIN_EMAIL', true),
        'azure'  => env('AUTH_LOGIN_AZURE', false),
        'bamboo' => env('AUTH_LOGIN_BAMBOO', false),
    ],
    'upload'                    => [
        // Disk in `config/filesystem.php`.
        'disk'      => 'admin',
        // Image and file upload path under the disk above.
        'directory' => [
            'image' => 'images',
            'file'  => 'files',
        ],
    ],
    'database'                  => [
        // Database connection for following tables.
        'connection'             => '',
        // User tables and model.
        'users_table'            => 'admin_users',
        'users_model'            => Bamboo\Admin\Auth\Database\Administrator::class,
        // Role table and model.
        'roles_table'            => 'admin_roles',
        'roles_model'            => Bamboo\Admin\Auth\Database\Role::class,
        // Permission table and model.
        'permissions_table'      => 'admin_permissions',
        'permissions_model'      => Bamboo\Admin\Auth\Database\Permission::class,
        // Menu table and model.
        'menu_table'             => 'admin_menu',
        'menu_model'             => Bamboo\Admin\Auth\Database\Menu::class,

        // Pivot table for table above.
        'operation_log_table'    => 'admin_operation_log',
        'user_permissions_table' => 'admin_user_permissions',
        'role_users_table'       => 'admin_role_users',
        'role_permissions_table' => 'admin_role_permissions',
        'role_menu_table'        => 'admin_role_menu',
        'translations_table'     => 'admin_translations',
    ],
    'operation_log'             => [
        'enable'          => true,
        'allowed_methods' => [
            'GET',
            'HEAD',
            'POST',
            'PUT',
            'DELETE',
            'CONNECT',
            'OPTIONS',
            'TRACE',
            'PATCH'
        ],
        'except'          => [
            'auth/logs*',
        ],
    ],
    'fields'                    => [
        'summernote' => [
            'lang'   => 'vi-VN',
            'height' => 250,
        ]
    ],
    'translations'              => [
        'delete_enabled'  => true,
        'exclude_groups'  => [],
        'exclude_langs'   => [],
        'sort_keys '      => false,
        'trans_functions' => [
            'trans',
            'trans_choice',
            'Lang::get',
            'Lang::choice',
            'Lang::trans',
            'Lang::transChoice',
            '@lang',
            '@choice',
            '__',
            '$trans.get',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | User default avatar
    |--------------------------------------------------------------------------
    |
    | Set a default avatar for newly created users.
    |
    */
    'default_avatar'            => admin_base_path('/bamboo-admin/AdminLTE/dist/img/user2-160x160.jpg'),

    /*
    |--------------------------------------------------------------------------
    | Admin map field provider
    |--------------------------------------------------------------------------
    |
    | Supported: "tencent", "google", "yandex".
    |
    */
    'map_provider'              => 'google',

    /*
    |--------------------------------------------------------------------------
    | Application Skin
    |--------------------------------------------------------------------------
    |
    | This value is the skin of admin pages.
    | @see https://adminlte.io/docs/2.4/layout
    |
    | Supported:
    |    "skin-blue", "skin-blue-light", "skin-yellow", "skin-yellow-light",
    |    "skin-green", "skin-green-light", "skin-purple", "skin-purple-light",
    |    "skin-red", "skin-red-light", "skin-black", "skin-black-light".
    |
    */
    'skin'                      => 'skin-black-light',

    /*
    |--------------------------------------------------------------------------
    | Application layout
    |--------------------------------------------------------------------------
    |
    | This value is the layout of admin pages.
    | @see https://adminlte.io/docs/2.4/layout
    |
    | Supported: "fixed", "layout-boxed", "layout-top-nav", "sidebar-collapse",
    | "sidebar-mini".
    |
    */
    'layout'                    => [
        'sidebar-mini',
        'sidebar-collapse'
    ],

    /*
    |--------------------------------------------------------------------------
    | Login page background image
    |--------------------------------------------------------------------------
    |
    | This value is used to set the background image of login page.
    |
    */
    'login_background_image'    => '',

    /*
    |--------------------------------------------------------------------------
    | Show version at footer
    |--------------------------------------------------------------------------
    |
    | Whether to display the version number of laravel-admin at the footer of
    | each page
    |
    */
    'show_version'              => false,

    /*
    |--------------------------------------------------------------------------
    | Show environment at footer
    |--------------------------------------------------------------------------
    |
    | Whether to display the environment at the footer of each page
    |
    */
    'show_environment'          => false,

    /*
    |--------------------------------------------------------------------------
    | Menu bind to permission
    |--------------------------------------------------------------------------
    |
    | whether enable menu bind to a permission
    */
    'menu_bind_permission'      => false,

    /*
    |--------------------------------------------------------------------------
    | Enable default breadcrumb
    |--------------------------------------------------------------------------
    |
    | Whether enable default breadcrumb for every page content.
    */
    'enable_menu_breadcrumb'    => true,
    'enable_default_breadcrumb' => false,

    /*
    |--------------------------------------------------------------------------
    | Enable/Disable assets minify
    |--------------------------------------------------------------------------
    */
    'minify_assets'             => true,

    /*
    |--------------------------------------------------------------------------
    | Enable/Disable sidebar menu search
    |--------------------------------------------------------------------------
    */
    'enable_menu_search'        => false,

    /*
    |--------------------------------------------------------------------------
    | Extension Directory
    |--------------------------------------------------------------------------
    |
    | When you use command `php artisan admin:extend` to generate extensions,
    | the extension files will be generated in this directory.
    */
    'extension_dir'             => app_path('Admin/Extensions'),

    /*
    |--------------------------------------------------------------------------
    | Settings for extensions.
    |--------------------------------------------------------------------------
    |
    | You can find all available extensions here
    | https://github.com/laravel-admin-extensions.
    |
    */
    'extensions'                => [

    ],
];
