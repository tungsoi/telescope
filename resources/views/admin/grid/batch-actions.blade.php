<input type="checkbox" class="{{ $selectAllName }}" />&nbsp;

@if(!$isHoldSelectAllCheckbox)
<div class="btn-group">
    <button class="btn btn-{{ config('admin.form-style') }} btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ trans('admin.action') }} <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        @foreach($actions as $action)
            <li><a href="#" class="{{ $action->getElementClass(false) }}">{{ $action->getTitle() }}</a></li>
        @endforeach
    </ul>
</div>
@endif